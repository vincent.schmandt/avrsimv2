#include "common/avrsimv2.h"


int main(int argc, char *argv[]) {
    avrsimv2_t avrsimv2;
    memset(&avrsimv2, 0, sizeof(avrsimv2_t));
    avrsimv2_setupCLI(&avrsimv2, &argc, &argv);
    avrsimv2_init(&avrsimv2);
    avrsimv2_main(&avrsimv2);
    avrsimv2_exit(&avrsimv2, EXIT_SUCCESS);
}