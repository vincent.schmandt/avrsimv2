#ifndef _LED_GL_H
#define _LED_GL_H

#include <stdint.h>

#include "led.h"
#include "../common/gl_utils.h"

GLvec2f led_gl_getSize(void);
void led_gl_draw(led_t *b, uint32_t color);

#endif //_LED_GL_H
